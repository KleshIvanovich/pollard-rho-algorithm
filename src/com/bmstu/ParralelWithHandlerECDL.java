package com.bmstu;

import com.bmstu.ec.ECMath;
import com.bmstu.ec.EllipticCurve;
import com.bmstu.ec.Point;

import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ParralelWithHandlerECDL implements DLCalculator {
    private final int numberOfThreads;
    private ExecutorService executor;
    private final Random randomGenerator = new Random();

    private final ConcurrentLinkedQueue<CollisionCandidate> concurrentLinkedQueue = new ConcurrentLinkedQueue();
    private Long logarithm;
    private long orderOfCurve;

    public ParralelWithHandlerECDL(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    @Override
    public void calculate(Point P, Point Q, EllipticCurve E, Listener listener) {
        this.executor = Executors.newFixedThreadPool(numberOfThreads);
        final CountDownLatch countDownLatch = new CountDownLatch(1);

        for (int i = 0; i < numberOfThreads - 1; ++i) {
            executor.execute(() -> {
                singleCalculation(P, Q, E);
            });
        };
        while (true) {
            CollisionCandidate candidate = concurrentLinkedQueue.peek();
            if (candidate == null) {
                continue;
            }

            long logarithm = candidate.checkForCollision();

            if (logarithm == -1) {
                continue;
            }

            listener.onCalculate(logarithm);
            return;
        }
}

    public void singleCalculation(Point P, Point Q, EllipticCurve E) {
        orderOfCurve = E.order();
        for (int i = 0; i <= 2; ++i) {
            long a_i = generateRandomInteger();
            long b_i = generateRandomInteger();

            long a_2i = generateRandomInteger();
            long b_2i = generateRandomInteger();


            Point a_iP = P.mult(a_i, E.getA(), E.getB(), E.getP());
            Point b_iQ = Q.mult(b_i, E.getA(), E.getB(), E.getP());
            Point X_i = a_iP
                    .add(b_iQ, E.getA(), E.getB(), E.getP());


            Point a_2iP = P.mult(a_2i, E.getA(), E.getB(), E.getP());
            Point b_2iQ = Q.mult(b_2i, E.getA(), E.getB(), E.getP());
            Point X_2i = a_2iP
                    .add(b_2iQ, E.getA(), E.getB(), E.getP());

            int j = 0;

            while (j < orderOfCurve) {
                if (X_i.equals(X_2i)) {
                    concurrentLinkedQueue.offer(new CollisionCandidate(X_i, X_2i, a_i, a_2i, b_i, b_2i, orderOfCurve));
                } else {
                    j += 1;
                }
                a_i = updateA_i(a_i, P, X_i, E);
                b_i = updateB_i(b_i, P, X_i, E);
                X_i = updateX_i(X_i, P, Q, E);

                Point updatedX_i = updateX_i(X_2i, P, Q, E);
                a_2i = updateA_i(updateA_i(a_2i, P, X_2i, E), P, updatedX_i, E);
                b_2i = updateB_i(updateB_i(b_2i, P, X_2i, E), P, updatedX_i, E);
                X_2i = updateX_i(updatedX_i, P, Q, E);
            }
        }
    }

    private long generateRandomInteger() {
        return (randomGenerator.nextInt((int) orderOfCurve - 2) + 4);
    }

    private long updateA_i(long a_i, Point prime, Point X_i, EllipticCurve E) {
        int subsetIndex = (int) (X_i.getX() % 3);
        switch (subsetIndex) {
            case 0:
                return (2 * a_i % orderOfCurve);
            case 1:
                return ((a_i + 1) % orderOfCurve);
            case 2:
                return a_i;
            default:
                return -1;
        }
    }

    private long updateB_i(long b_i, Point prime, Point X_i, EllipticCurve E) {
        int subsetIndex = (int) (X_i.getX() % 3);
        switch (subsetIndex) {
            case 0:
                return ((2 * b_i) % orderOfCurve);
            case 1:
                return b_i;
            case 2:
                return ((b_i + 1) % orderOfCurve);
            default:
                return -1;
        }
    }

    private Point updateX_i(Point X_i, Point P, Point Q, EllipticCurve E) {
        int subsetIndex = (int) (X_i.getX() % 3);
        switch (subsetIndex) {
            case 0:
                return X_i.mult(2, E.getA(), E.getB(), E.getP());
            case 1:
                return X_i.add(P, E.getA(), E.getB(), E.getP());
            case 2:
                return X_i.add(Q, E.getA(), E.getB(), E.getP());
            default:
                return null;
        }
    }

private static class CollisionCandidate {
    private Point x_i;
    private Point x_2i;
    private long a_i;
    private long a_2i;
    private long b_i;
    private long b_2i;
    private long n;

    CollisionCandidate(Point x_i, Point x_2i, long a_i, long a_2i, long b_i, long b_2i, long n) {
        this.x_i = x_i;
        this.x_2i = x_2i;
        this.a_i = a_i;
        this.a_2i = a_2i;
        this.b_i = b_i;
        this.b_2i = b_2i;
        this.n = n;
    }

    long checkForCollision() {
        if (b_i == b_2i) {
            return -1;
        }

        if (ECMath.gcd(b_i, b_2i) != 1) {
            return -1;
        }

        long a_diff = ((a_i - a_2i) % n);
        long b_diff = ((b_2i - b_i) % n);
        long b_diff_reverse = ECMath.inverse(b_diff, n);
        long logarithm = ((a_diff * b_diff_reverse) % n);
        if (logarithm < 0) {
            logarithm += n;
        }

        return logarithm;

    }
}
}
