package com.bmstu;

import com.bmstu.ec.EllipticCurve;
import com.bmstu.ec.Point;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {
    private static long x;
    public static void main(String[] args) {
        System.out.println("Generating EC...");
        EllipticCurve ellipticCurve = EllipticCurve.generateRandomPrimeCurve(773);
        Point P = ellipticCurve.getRandomPoint();
        System.out.println(ellipticCurve.toString());
        System.out.println(P);

        Random random = new Random();
        x = random.nextInt((int)ellipticCurve.order() - 2) + 4;
        Point Q = P.mult(x, ellipticCurve.getA(), ellipticCurve.getB(), ellipticCurve.getP());
        System.out.println("Order of curve:" + ellipticCurve.order());
        System.out.println("Multiply P = " + P + " by x=" + x + ", got Q = " + Q);

        DLCalculator calculator = new BaseECDLCalculator();
        long sequentStartTime = System.currentTimeMillis();
        calculator.calculate(P, Q, ellipticCurve, calculated_x -> {
            long stopTime = System.currentTimeMillis() - sequentStartTime;
            System.out.println("Time elapsed:" + stopTime);
            System.out.println("Expected: " + x + " Got:" + calculated_x);
            Point gotQ = P.mult(calculated_x, ellipticCurve.getA(), ellipticCurve.getB(), ellipticCurve.getP());
            if (x != calculated_x) {
                throw new IllegalStateException();
            }
        });

        DLCalculator parallelCalculator = new ParralelECDLCalculator(2);
        long parallelStartTime = System.currentTimeMillis();
        parallelCalculator.calculate(P, Q, ellipticCurve, calculated_x -> {
            long stopTime = System.currentTimeMillis() - parallelStartTime;
            System.out.println("Parallel, time elapsed:" + stopTime);
            Point gotQ = P.mult(calculated_x, ellipticCurve.getA(), ellipticCurve.getB(), ellipticCurve.getP());
            if (x != calculated_x) {
                throw new IllegalStateException();
            }
        });

        DLCalculator parallelHandlerCalculator = new ParralelWithHandlerECDL(2);
        long parallelHandlerStartTime = System.currentTimeMillis();
        parallelHandlerCalculator.calculate(P, Q, ellipticCurve, calculated_x -> {
            long stopTime = System.currentTimeMillis() - parallelHandlerStartTime;
            System.out.println("Parallel with handler, time elapsed:" + stopTime);
            Point gotQ = P.mult(calculated_x, ellipticCurve.getA(), ellipticCurve.getB(), ellipticCurve.getP());
            if (x != calculated_x) {
                throw new IllegalStateException();
            }
        });

//        BenchMark sequentBenchmark = new BenchMark(calculator);
//        BenchMark parallelBenchmark = new BenchMark(parallelCalculator);
//
//        System.out.println("Sequent:" + sequentBenchmark.averageTime(127, 50));
//        System.out.println("Parralel:" + parallelBenchmark.averageTime(127, 50));


    }

    private static class BenchMark {
        private final DLCalculator calculator;
        private final List<Long> measures = new LinkedList<>();
        private long totalTime = 0;

        private BenchMark(DLCalculator calculator) {
            this.calculator = calculator;
        }

        float averageTime(int prime, int numberOfLaunches) {
            totalTime = 0;

            for(int i = 0; i < numberOfLaunches; ++i) {
                EllipticCurve ellipticCurve = EllipticCurve.generateRandomPrimeCurve(prime);
                Point P = ellipticCurve.getRandomPoint();
                System.out.println(ellipticCurve.toString());
                System.out.println(P);

                Random random = new Random();
                long pow = random.nextInt((int)ellipticCurve.order() - 2) + 4;
                Point Q = P.mult(pow, ellipticCurve.getA(), ellipticCurve.getB(), ellipticCurve.getP());

                long startTime = System.currentTimeMillis();
                calculator.calculate(P, Q, ellipticCurve, calculated_x -> {
                    long stopTime = System.currentTimeMillis() - startTime;
                    if (pow != calculated_x) {
                        throw new IllegalStateException();
                    }
                    totalTime += stopTime;
                });
            }

            return (float) totalTime / numberOfLaunches;
        }
    }
}
