package com.bmstu;

import com.bmstu.ec.EllipticCurve;
import com.bmstu.ec.Point;

public interface DLCalculator {

    void calculate(Point P, Point Q, EllipticCurve E, Listener listener);

    interface Listener {
        void onCalculate(long x);
    }
}